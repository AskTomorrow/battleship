Для разработки я использовал не свой код для HW1, а немного другой подход к реализации морского боя,
поэтому у меня в проекте не такие классы, как были заявлены в HW1

При наведении на клетку поля она меняет цвет
при выстреле она становится голубой, если выстрел мимо, и красной, если выстрел пришелся на корабль

все выстрелы выводятся в панель логов справа, в формате:
координаты_результат

количество выстрелов также подсчитывается в специальной неизменяемой панели

если корабль был потоплен, его картинка(справа) исчезнет

для альтернативного ввода используются две текстовые формы для координат X и Y соответственно,
для переключения между ними используется клавиша Tab, для ввода - клавиша Enter
при некоректном вводе игроку будет сообщен формат ввода еще раз, это сообщение пропадет при корректном вводе,
и снова выпадет при некорректном

При повторном нажатии на уже "открытую" клетку, игроку об этом сообщится

При уничтожении всех кораблей игроку выведется сообщение о победе

Good Luck, Have Fun!