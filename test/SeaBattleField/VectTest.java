package SeaBattleField;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectTest {

    @Test
    void eq() {
        Vect c = new Vect(new Ship(2, 3));
        assertEquals(true, c.eq(2, 3));
    }

    @Test
    void getXy() {
        Vect v = new Vect(new Ship(2, 3));

        Ship ship = new Ship(2, 5);
        v.setXy(ship);
        assertEquals(ship, v.getXy());
    }

    @Test
    void setXy() {
        Ship ship1 = new Ship(2, 2);
        Vect v = new Vect(ship1);

        Ship ship = new Ship(3, 3);
        v.setXy(ship);
        assertEquals(ship, v.getXy());

    }
}