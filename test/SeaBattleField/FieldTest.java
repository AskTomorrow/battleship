package SeaBattleField;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FieldTest {

    @Test
    void checkBound() {
        Ship ship1 = new Ship(1, 1);
        Ship ship = new Ship(100, 100);

        Field field = new Field();

        assertEquals(true, field.checkBound(ship1));
        assertEquals(false, field.checkBound(ship));
    }
}