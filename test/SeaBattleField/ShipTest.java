package SeaBattleField;

import static org.junit.jupiter.api.Assertions.*;
import SeaBattleField.*;

class ShipTest {

    @org.junit.jupiter.api.Test
    void add() {
        Vect v = new Vect(new Ship(2, 3));
        Ship ship = new Ship(7, 7);

        ship = ship.add(v);

        assertEquals(9, ship.getX());
        assertEquals(10, ship.getY());

    }

    @org.junit.jupiter.api.Test
    void getX() {
        Ship ship = new Ship(2, 3);
        assertEquals(2, ship.getX());
    }

    @org.junit.jupiter.api.Test
    void getY() {
        Ship ship = new Ship(2, 3);
        assertEquals(3, ship.getY());
    }

    @org.junit.jupiter.api.Test
    void setX() {
        Ship ship = new Ship(2, 3);
        ship.setX(5);

        assertEquals(5, ship.getX());
    }

    @org.junit.jupiter.api.Test
    void setY() {
        Ship ship = new Ship(2, 3);
        ship.setY(5);

        assertEquals(5, ship.getY());
    }

    @org.junit.jupiter.api.Test
    void setXY() {
        Ship ship = new Ship(2, 3);
        ship.setXY(5, 7);

        assertEquals(7, ship.getY());
        assertEquals(5, ship.getX());
    }

    @org.junit.jupiter.api.Test
    void testAdd() {
        Ship ship = new Ship(2, 2);
        ship = ship.add(3, 3);

        assertEquals(5, ship.getX());
        assertEquals(5, ship.getY());
    }
}