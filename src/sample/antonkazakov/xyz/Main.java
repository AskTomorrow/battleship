package sample.antonkazakov.xyz;

import SeaBattleField.Field;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {

    private int countOfShots = 0;
    private MyButton[][] shipButtons = new MyButton[10][10];
    private static Field field = new Field(10, 10);
    private static TextArea counterField;
    private static TextArea logField;
    private static boolean[][] isAlreadyPressed = new boolean[10][10];
    private static String logText = "";
    private static ArrayList<ImageView> _1CellsShips = new ArrayList<>();
    private static ArrayList<ImageView> _2CellsShips = new ArrayList<>();
    private static ArrayList<ImageView> _3CellsShips = new ArrayList<>();
    private static ArrayList<ImageView> _4CellsShips = new ArrayList<>();
    private static ArrayList<ArrayList<ImageView>> listOfImagesOfShips = new ArrayList<>();
    private static TextField textX;
    private static TextField textY;
    private static TextArea message;

    private void addTo4(AnchorPane anchorPane) {
        ImageView image = (ImageView) anchorPane.getChildren().get(3);
        _4CellsShips.add(image);
    }

    private void addTo3(AnchorPane anchorPane) {
        for (int i = 0; i < 2; i++) {
            ImageView image = (ImageView) anchorPane.getChildren().get(4 + i);
            _3CellsShips.add(image);
        }
    }

    private void addTo2(AnchorPane anchorPane) {
        for (int i = 0; i < 3; i++) {
            ImageView image = (ImageView) anchorPane.getChildren().get(6 + i);
            _2CellsShips.add(image);
        }
    }

    private void addTo1(AnchorPane anchorPane) {
        for (int i = 0; i < 4; i++) {
            ImageView image = (ImageView) anchorPane.getChildren().get(9 + i);
            _1CellsShips.add(image);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Battleship Game");
        primaryStage.setScene(new Scene(root, 500, 500));
        primaryStage.show();

        AnchorPane anchorPane = ((AnchorPane) root);

        GridPane gridPane = (GridPane) anchorPane.getChildren().get(0);
        TextArea textArea = (TextArea) anchorPane.getChildren().get(2);
        TextArea currLogField = (TextArea) anchorPane.getChildren().get(17);

        TextField textFieldX = (TextField) anchorPane.getChildren().get(21);
        TextField textFieldY = (TextField) anchorPane.getChildren().get(22);

        message = (TextArea) anchorPane.getChildren().get(24);
        message.setEditable(false);
        message.setVisible(false);

        textX = textFieldX;
        textY = textFieldY;

        textFieldX.setOnAction(this::enterTextX);
        textFieldY.setOnAction(this::enterTextY);

        addTo1(anchorPane);
        addTo2(anchorPane);
        addTo3(anchorPane);
        addTo4(anchorPane);

        listOfImagesOfShips.add(_1CellsShips);
        listOfImagesOfShips.add(_2CellsShips);
        listOfImagesOfShips.add(_3CellsShips);
        listOfImagesOfShips.add(_4CellsShips);

        counterField = textArea;
        logField = currLogField;

        counterField.setStyle("-fx-text-fill: black");
        counterField.setEditable(false);

        field.fill();

        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < 10; i++) {
                MyButton myButton = new MyButton(j, i);

                myButton.setPrefWidth(90);
                myButton.setPrefHeight(90);

                myButton.setStyle("-fx-background-color: lightgrey; -fx-border-width: 1px; -fx-border-color: gray");

                shipButtons[j][i] = myButton;
                myButton.setOnMouseClicked(this::clickedEvent);
                myButton.setOnMouseEntered(this::hoverEvent);
                myButton.setOnMouseExited(this::exitEvent);

                gridPane.add(shipButtons[j][i], j, i);
            }
        }

        textArea.setText(Integer.toString(countOfShots));
        textArea.setStyle("-fx-background-color: black");
    }

    private boolean isNumber(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) < '0' || s.charAt(i) > '9') {
                return false;
            }
        }
        return true;
    }

    private void clearAll() {
        textY.clear();
        textX.clear();
    }

    private void enterTextX(ActionEvent actionEvent) {
        if (actionEvent != null) {
            TextField X = (TextField) actionEvent.getSource();

            if (isNumber(X.getText())) {
                int textInX = Integer.parseInt(X.getText()) - 1;
                int textInY = Integer.parseInt(textY.getText()) - 1;

                if (textInX >= 0 && textInX <= 9 && textInY >= 0 && textInY <= 9) {
                    makeTurn(shipButtons[textInY][textInX]);
                    message.setVisible(false);
                    clearAll();
                } else {
                    message.setVisible(true);
                    clearAll();
                }
            } else {
                message.setVisible(true);
                clearAll();
            }
        }
    }

    private void enterTextY(ActionEvent actionEvent) {
        if (actionEvent != null) {
            TextField Y = (TextField) actionEvent.getSource();

            if (isNumber(Y.getText())) {
                int textInY = Integer.parseInt(Y.getText()) - 1;
                int textInX = Integer.parseInt(textX.getText()) - 1;

                if (textInX >= 0 && textInX <= 9 && textInY >= 0 && textInY <= 9) {
                    makeTurn(shipButtons[textInY][textInX]);
                    message.setVisible(false);
                    clearAll();
                } else {
                    message.setVisible(true);
                    clearAll();
                }
            } else {
                message.setVisible(true);
                clearAll();
            }
        }
    }

    private void exitEvent(MouseEvent mouseEvent) {
        if (mouseEvent != null) {
            MyButton btn = (MyButton) mouseEvent.getSource();
            if (btn.getState() == 0) {
                btn.setStyle("-fx-background-color: lightgrey; -fx-border-width: 1px; -fx-border-color: gray");
            }
        }
    }

    private void hoverEvent(MouseEvent mouseEvent) {
        if (mouseEvent != null) {
            MyButton btn = (MyButton) mouseEvent.getSource();
            if (btn.getState() == 0) {
                btn.setStyle("-fx-background-color: orange; -fx-border-width: 1px; -fx-border-color: orange");
            }
        }
    }

    private void clickedEvent(MouseEvent mouseEvent) {
        if (mouseEvent != null) {
            MyButton btn = (MyButton) mouseEvent.getSource();
            makeTurn(btn);
        }
    }

    private void makeTurn(MyButton buttonToTurn) {
        if (buttonToTurn != null) {
            if (buttonToTurn.getState() != 0) {
                Alert countMessage = new Alert(Alert.AlertType.INFORMATION);
                countMessage.setTitle("THE CELL IS UNDER ATTACK");
                countMessage.setHeaderText("YOU HAVE ALREADY SHOOT AT THIS CELL, BRO.\n" +
                        "CHOOSE ANOTHER ONE");
                countMessage.setContentText("HOPE YOU ARE GOING TO DO A RIGHT CHOICE HE-HE");
                countMessage.show();
            } else {

                int coordX = buttonToTurn.getxCoord();
                int coordY = buttonToTurn.getyCoord();

                String res = "";
                int resultOfShoot = field.shoot(coordX, coordY);

                if (resultOfShoot > -1) {
                    buttonToTurn.setStyle("-fx-background-color: red;  -fx-border-width: 1px; -fx-border-color: black");
                    buttonToTurn.setState(2);

                    if (resultOfShoot > 0) {
                        res = resultOfShoot + " cell(s) ship sinked";

                        ArrayList<ImageView> imageViewForShoot = listOfImagesOfShips.get(resultOfShoot - 1);
                        ImageView imageView = imageViewForShoot.get(imageViewForShoot.size() - 1);
                        imageView.setVisible(false);

                        listOfImagesOfShips.get(resultOfShoot - 1).remove(imageViewForShoot.size() - 1);
                    } else {
                        res = "some kind of ship is being damaged";
                    }

                } else {
                    buttonToTurn.setStyle("-fx-background-color: lightblue;  -fx-border-width: 1px; -fx-border-color: black");
                    buttonToTurn.setState(1);
                    res = "miss";
                }

                if (!isAlreadyPressed[coordX][coordY]) {
                    countOfShots++;
                    isAlreadyPressed[coordX][coordY] = true;
                }

                counterField.setText(Integer.toString(countOfShots));
                logText += "X = " + (coordY + 1) + " " + "Y = " + (coordX + 1) + " " + res + "\n";

                logField.setText(logText);

                if (checkIsAllSunk() || countOfShots == 100) {
                    Alert countMessage = new Alert(Alert.AlertType.WARNING);
                    countMessage.setTitle("WINNER");
                    countMessage.setHeaderText("CONGRATULATIONS!!!!!!\n" +
                            "YOU WIN!!!!");
                    countMessage.setContentText("YOU ARE THE GREATEST PLAYER I'VE EVER SEEN!");
                    countMessage.show();
                }
            }
        }
    }

    private boolean checkIsAllSunk() {
        for (ArrayList<ImageView> listOfImagesOfShip : listOfImagesOfShips) {
            if (listOfImagesOfShip.size() > 0) {
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        launch(args);
    }
}
