package sample.antonkazakov.xyz;

import javafx.scene.control.Button;

public class MyButton extends Button {
    public int getxCoord() {
        return xCoord;
    }

    public void setxCoord(int xCoord) {
        this.xCoord = xCoord;
    }

    private int xCoord;

    public int getyCoord() {
        return yCoord;
    }

    public void setyCoord(int yCoord) {
        this.yCoord = yCoord;
    }

    private int yCoord;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    int state;

    MyButton(int x, int y) {
        super("");

        this.xCoord = x;
        this.yCoord = y;
        this.state = 0;
    }

}
