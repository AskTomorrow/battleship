package SeaBattleField;

import java.util.Vector;

public class Field {
    /** само поле и его размеры**/
    private int[][] a;
    private int nSize;
    private int mSize;
    private int[][] used;

    /** направления заданные векторами (0;1), (1;0), (-1;0), (0;-1)**/
    static private Vector<Vect> dirs = new Vector<>();

    /**Конструктор поле n*m**/
    public Field(int n, int m) {
        dirs.add(new Vect(new Ship(0, 1)));
        dirs.add(new Vect(new Ship(1, 0)));
        dirs.add(new Vect(new Ship(0, -1)));
        dirs.add(new Vect(new Ship(-1, 0)));

        this.a = new int[n][m];
        this.used = new int[n][m];
        this.nSize = n;
        this.mSize = m;

        for (int i = 0; i < this.nSize; ++i) {
            for (int j = 0; j < this.mSize; ++j) {
                used[i][j] = 0;
                a[i][j] = 0;
            }
        }
    }

    /**Конструктор по умолчанию**/
    public Field() {
        dirs.add(new Vect(new Ship(0, 1)));
        dirs.add(new Vect(new Ship(1, 0)));
        dirs.add(new Vect(new Ship(0, -1)));
        dirs.add(new Vect(new Ship(-1, 0)));

        this.a = new int[10][10];
        this.nSize = 10;
        this.mSize = 10;

        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 10; ++j) {
                a[i][j] = 0;
            }
        }
    }

    /**Проверка того, что точка в пределах поля**/
    public boolean checkBound(Ship cur) {
        if (cur == null) {
            return false;
        }
        return (cur.getX() >= 0 && cur.getY() >= 0 && cur.getX() < nSize && cur.getY() < mSize);
    }

    /**проверяет что вокруг точки везде нули (сама точка тоже учитывается)**/
    public boolean checkSpace(Ship cur) {
        Vector <Ship> dxy = new Vector<>();
        for (int i = -1; i < 2; ++i) {
            for (int j = -1; j < 2; ++j) {
                dxy.add(cur.add(i, j));
            }
        }

        for (Ship i: dxy) {
            if (checkBound(i)) {
                if (this.a[i.getX()][i.getY()] != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**Проверка что начиная с точки cur в направлении v мы можем поставить корабль длинной len**/
    public boolean checkPlaceForShip(int len, Ship cur, Vect v) {
        if (cur == null) {
            return false;
        }
        Ship finish = cur.add(v.multiply(len));
        if (!checkBound(finish) || this.a[finish.getX()][finish.getY()] != 0) {
            return false;
        }

        for (int i = 0; i < len; ++i, cur = cur.add(v)) {
            if (!checkSpace(cur) || !checkBound(cur)) {
                return false;
            }
        }
        return true;
    }

    /**Записываем корабль длиной len в направлении v, начиная с точки cur**/
    public void pshShip(int len, Ship cur, Vect v) {
        for(int i = 0; i < len; ++i, cur = cur.add(v)) {
            a[cur.getX()][cur.getY()] = len;
        }
    }


    /**Проверяет Можем ли мы положить корабль длиной len из точки start
     * в каком-нибудь направлении,
     * по возможности ставить этот корабль**/
    public boolean checkAll(int len, Ship start) {
        if (start == null) {
            return false;
        }
        if(this.a[start.getX()][start.getY()] != 0) {
            return false;
        }
        /**чёткий цикл
         * всё ради того чтобы направление было случайно**/
        for (int i = ((int)Math.random()) % 10, cnt = 0; cnt < dirs.size(); ++cnt, i = (i + 1) % dirs.size()) {
            if (checkPlaceForShip(len, start, dirs.elementAt(i))) {
                pshShip(len, start, dirs.elementAt(i));
                return true;
            }
        }
        return false;
    }

    /**находит сумму вокруг точки**/
    public int sumAround(Ship ship) {
        int res = 0;
        for (int i = -1; i < 2; ++i) {
            for (int j = -1; j < 2; ++j) {
                if (checkBound(ship.add(i, j))) {
                    if (i != 0 || j != 0) {
                        res += used[ship.getX() + i][ship.getY() + j];
                    }
                }
            }
        }
        return res;
    }

    /**проталкивает значение по всем открытым рядом клеткам**/
    public void updateUsed(Ship ship, int num) {
        if (ship != null) {
            used[ship.getX()][ship.getY()] = num;
            for (int i = -1; i < 2; ++i) {
                for (int j = -1; j < 2; ++j) {
                    if (checkBound(ship.add(i, j)) &&
                            used[ship.getX() + i][ship.getY() + j] > 0 &&
                            used[ship.getX() + i][ship.getY() + j] != num) {
                        updateUsed(ship.add(i, j), num);
                    }
                }
            }
        }
    }

    /**Выстрел в точку. Возвращаем:
     * если убил- размер коробля(от 1 и более)
     * если ранили- 0
     * если там пусто- -1
     * **/
    public int shoot(int coordX, int coordY) {

        if (a[coordX][coordY] == 0) {
            return -1;
        }

        System.out.println();
        Ship current = new Ship(coordX, coordY);
        int newNum = sumAround(current) + 1;
        updateUsed(current, newNum);

        if (used[coordX][coordY] == a[coordX][coordY]) {
            return a[coordX][coordY];
        }
        return 0;
    }

    /**пытаемся добавить на поле корабль длиной len, если ок то возвращаем true иначе false**/
    public boolean set(int len) {
        if (Math.max(nSize, mSize) < len) {
            return false;
        }

        Ship cur = new Ship(Math.min(nSize, mSize));

        /**чёткий цикл**/
        for (int cnt = 0; cnt < nSize*mSize || !checkAll(len, cur); ++cnt, cur.setRandom(Math.min(nSize, mSize)));

        return false;
    }

    /**выводим поле**/
    public void print() {
        for (int i = 0; i < nSize; ++i) {
            for (int j = 0; j < mSize; ++j) {
                System.out.print(a[i][j]);
                System.out.print(' ');
            }
            System.out.println();
        }
    }

    /** заполняем поле кораблями длиной i**/
    public void fill() {
        for (int i = 1; i <= 4; ++i) {
            for (int j = i; j <= 4; ++j) {
                set(i);
            }
        }
    }

    public int[][] getA() {
        return a;
    }

    public int getMSize() {
        return mSize;
    }

    public int getNSize() {
        return nSize;
    }
}
