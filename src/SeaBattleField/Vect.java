package SeaBattleField;

public class Vect {
    private Ship xy;

    public Vect(Ship Ship) {
        this.xy = Ship;
    }

    /**сравнивает вектор по координатам**/
    public boolean eq(double x, double y) {
        return(this.xy.getX() == x && this.xy.getY() == y);
    }

    /**возвращает координаты**/
    public Ship getXy() {
        return xy;
    }

    /**умножение вектора на число**/
    public Vect multiply(int q) {
        return (new Vect(this.xy.multiply(q)));
    }

    /**установить координаты**/
    public void setXy(Ship xy) {
        this.xy = xy;
    }
}
