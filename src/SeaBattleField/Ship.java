package SeaBattleField;

import java.util.Random;

import static java.lang.StrictMath.abs;

public class Ship {
    private int x;
    private int y;

    Ship(int size) {
        this.setRandom(size);
    }

    public Ship(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**выводим точку**/
    public void print() {
        System.out.println("X:" + this.x + "   Y:" + this.y + '\n');
    }

    /**возвращаем точку сдвинутую на вектор с помощью вектора**/
    public Ship add(Vect v) {
        if (v == null) {
            return null;
        }
        return new Ship(this.x + v.getXy().getX(), this.y + v.getXy().getY());
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**Рандомим точку [0, size) **/
    void setRandom(int size) {
        Random rand = new Random();
        this.x = abs(rand.nextInt()) % size;
        this.y = abs(rand.nextInt()) % size;
    }

    /**умножаем точку(как ветор)**/
    Ship multiply(int q) {
        return(new Ship(this.x * q,this.y * q));
    }

    /**возвращаем точку, сдвинутую на вектор**/
    public Ship add(int i, int i1) {
        return new Ship(this.x + i, this.y + i1);
    }
}
